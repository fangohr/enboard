Enboard is a terminal UI to easily switch between conda environments.

To install::

    pip install enboard

To use::

    enboard
